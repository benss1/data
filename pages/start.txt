====== Wiki für die Nacht der Wissenschaft in Siegen ======
Willkommen im Wiki für die NdWS. 

===== Anmerkungen =====
  * Bitte arbeitet mit Kategorien und Namespaces. Genaueres findet ihr hier: [[https://www.dokuwiki.org/de:namespaces]] 
  * Zusätzlich erstellt soviele Referenzverweise wie möglich, damit auch alle Artikel gefunden werden können.
  * Einleitungsmaterial findet ihr hier: [[https://www.dokuwiki.org/de:manual]]
  * Das Wiki wird regelmäßig in eine git repo gesichert, falls es also Probleme oder so gibt, existiert ein Backup. 

===== Inhalte =====
Übergeordnete Kategorien könnten zum Beispiel sein:
  * Idee der Nacht der Wissenschaft
  * Pressearbeit
  * Location
   * Unordered
  * Verpflegung
  * Vortragende/Panels
  * Finanzen
  * Merchandise
  * Helfer